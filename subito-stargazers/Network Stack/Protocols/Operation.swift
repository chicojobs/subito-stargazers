//
//  Operation.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 05/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

protocol Operation {
    associatedtype U
    var request: URLRequest { get }
    func execute(in dispatcher: Dispatcher) -> Observable<U>
}
