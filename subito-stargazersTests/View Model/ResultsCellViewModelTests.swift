//
//  ResultsCellViewModelTests.swift
//  subito-stargazersTests
//
//  Created by Enrico Querci on 11/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
import RxSwift
import RxTest

class ResultsCellViewModelTests: XCTestCase {
    
    var disposeBag = DisposeBag()
    var scheduler: TestScheduler!
    var viewModel: ResultsCellViewModel!
    
    override func setUp() {
        super.setUp()
        self.scheduler = TestScheduler(initialClock: 0)
        let user = User(username: "Test User", avatar: "https://avatars1.githubusercontent.com/u/16398105?v=4")
        self.viewModel = ResultsCellViewModel(user: user)
    }
    
    func testUsername() {
        let stringObserver = scheduler.createObserver(String.self)
        scheduler.scheduleAt(0) {
            self.viewModel.username.drive(stringObserver).addDisposableTo(self.disposeBag)
        }
        
        scheduler.start()
        
        let expected = [
            next(0, "Test User"),
            completed(0)
        ]
        
        XCTAssertEqual(stringObserver.events, expected)
    }
}
