//
//  UserData.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 11/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct UserData {
    var users: [User]
    var next: String?
}
