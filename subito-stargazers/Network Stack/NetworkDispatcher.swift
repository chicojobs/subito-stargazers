//
//  NetworkDispatcher.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 05/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

class NetworkDispatcher: Dispatcher {
    func execute(request: URLRequest) -> Observable<Response> {
        return URLSession.shared.rx.response(request: request)
            .map { response -> Response in
                return Response((response.0, response.1, nil), for: request)
            }.catchError({ error in
                return Observable.just(Response((nil, nil, error), for: request))
            })
    }
}
