//
//  User.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 04/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct User: Codable {
    let username: String
    let avatar: String
    
    enum CodingKeys: String, CodingKey {
        case username = "login"
        case avatar = "avatar_url"
    }
}
