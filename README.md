# LEGGIMI #

Per aprire il workspace è necessario `Xcode 9` poiché la app sfrutta il nuovo protocollo `Codable` di `Swift 4`, che consente la deserializzazione di una risposta REST senza dover ricorrere a librerie aggiuntive (tipo ObjectMapper o SwiftyJson).

La app è basata sull’architettura *MVVM (Model, View, View Model)* che separa più chiaramente la presentazione dalla logica, evita la sindrome dei Massive View Controller e consente una maggiore testabilità.

Infine, ho deciso di usare `RxSwift` perché è lo strumento ideale per il binding ViewModel/View e rende molto più facile la propagazione alla View delle modifiche che avvengono nel ViewModel. In scenari asincroni come il networking, l’uso degli `Observable` evita di annidare svariati metodi con callback, ottenendo anche una gestione degli errori più immediata.

Per il networking, di solito utilizzo una variante dell'architettura suggerita nell'articolo **Network Layers in Swift** (https://medium.com/@danielemargutti/network-layers-in-swift-7fc5628ff789), che rafforza il principio di singola responsabilità.