//
//  UsersOperationTests.swift
//  subito-stargazersTests
//
//  Created by Enrico Querci on 12/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
import RxSwift
import RxTest

class UsersOperationTests: XCTestCase {
    
    var usersOperation: UsersOperation!
    
    override func setUp() {
        super.setUp()

        usersOperation = UsersOperation(owner: "test_user", repo: "test_repo")
    }
    
    func testRequest() {
        let mockRequest = URLRequest(url: URL(string: "https://api.github.com/repos/test_user/test_repo/stargazers")!)
        XCTAssertEqual(usersOperation.request, mockRequest)
        XCTAssertEqual(usersOperation.request.url!, mockRequest.url!)
    }
    
}
