//
//  ResultsTableViewController.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 07/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

class ResultsTableViewController: UITableViewController {

    private let disposeBag: DisposeBag = DisposeBag()
    private var dataSource: DataSource!
    
    var viewModel: ResultsTableViewModel?
    var repo: String!
    var owner: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60.0
        
        let networkDispatcher = NetworkDispatcher()
        dataSource = DataSource(dispatcher: networkDispatcher)
        
        dataSource.getUsersFrom(repo, withOwner: owner).subscribe(onNext: { data in
            guard data.users.count > 0 else {
                self.displayAlert()
                return
            }
            self.viewModel = ResultsTableViewModel(data: data)

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }).addDisposableTo(disposeBag)
    }

    private func displayAlert() {
        let alert = UIAlertAction(title: "OK", style: .default) { _ in
            self.navigationController?.popViewController(animated: true)
        }
        let controller = UIAlertController(title: "No results found", message: "No results found for the owner and repo provided. Please try with a different combination.", preferredStyle: .alert)
        controller.addAction(alert)
        present(controller, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel?.numberOfUsers ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "resultsCell", for: indexPath) as? ResultsCell else {
            fatalError("Could not find a cell of type ResultsCell")
        }
        guard let cellViewModel = viewModel?.cellViewModel(for: indexPath) else {
            fatalError("Could not retrieve a cell view model for given index path")
        }

        cell.selectionStyle = .none
        cell.viewModel = cellViewModel
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard viewModel?.nextLink != nil else { return nil }
        
        guard let footer = UINib(nibName: "ResultsTableFooter", bundle: Bundle.main).instantiate(withOwner: nil, options: nil).first as? ResultsTableFooter else { fatalError() }
        
        footer.moreResults.addTarget(self, action: #selector(moreResultsButtonPressed(_:)), for: .touchUpInside)
        
        return footer
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    @IBAction func moreResultsButtonPressed(_ sender: Any) {
        guard let link = viewModel?.nextLink else { return }

        dataSource.getUsersFrom(nextPage: link).subscribe(onNext: { data in
            guard data.users.count > 0 else { return }
            self.viewModel?.append(data: data)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }).addDisposableTo(disposeBag)
        
    }
}
