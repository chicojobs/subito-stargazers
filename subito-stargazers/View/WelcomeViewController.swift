//
//  WelcomeViewController.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 11/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

class WelcomeViewController: UIViewController {
    private let disposeBag: DisposeBag = DisposeBag()
    
    var viewModel: WelcomeViewControllerViewModel!
    
    @IBOutlet weak var ownerTextField: UITextField!
    @IBOutlet weak var repoTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        guard let owner = ownerTextField.text, let repo = repoTextField.text else { return }

        guard let resultsTableViewController = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: ResultsTableViewController.self)) as? ResultsTableViewController else { return }

        resultsTableViewController.repo = repo
        resultsTableViewController.owner = owner

        self.navigationController?.pushViewController(resultsTableViewController, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = WelcomeViewControllerViewModel(ownerTap: ownerTextField.rx.text.asDriver(), repoTap: repoTextField.rx.text.asDriver())
        
        viewModel.searchButtonEnabled.drive(searchButton.rx.isEnabled).addDisposableTo(disposeBag)
    }
    
}
