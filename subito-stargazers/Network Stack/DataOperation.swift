//
//  DataOperation.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 11/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

class DataOperation: Operation {
    typealias U = Data
    var urlString: String

    var request: URLRequest {
        return URLRequest(url: URL(string: urlString)!)
    }
    
    func execute(in dispatcher: Dispatcher) -> Observable<U> {
        return dispatcher.execute(request: request).map { response -> U in
            switch response {
                case .data(let data):
                    return data
                default:
                    return Data()
            }
        }
    }
    
    init(urlString: String) {
        self.urlString = urlString
    }
}
