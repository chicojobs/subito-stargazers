//
//  ResultsTableViewModel.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 10/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

class ResultsTableViewModel {
    private var data: UserData
    
    init(data: UserData) {
        self.data = data
    }
    
    // MARK: Output
    var numberOfUsers: Int { return data.users.count }
    var nextLink: String? { return data.next }
    
    func cellViewModel(for indexPath: IndexPath) -> ResultsCellViewModel? {
        let user = data.users[indexPath.row]
        return ResultsCellViewModel(user: user)
    }
    
    func append(data: UserData) {
        self.data.users.append(contentsOf: data.users)
        self.data.next = data.next
    }
}
