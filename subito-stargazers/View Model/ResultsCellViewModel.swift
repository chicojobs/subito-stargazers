//
//  ResultsCellViewModel.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 10/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

class ResultsCellViewModel {
    
    init(user: User) {
        self.username = Observable.just(user.username).asDriver(onErrorJustReturn: "")
        
        let dataSource = DataSource(dispatcher: NetworkDispatcher())
        self.avatar = dataSource.getImageFor(user.avatar).map { data in
            return UIImage(data: data)
            }.asDriver(onErrorJustReturn: nil)
    }
    
    // MARK: - Output
    var username: Driver<String>
    var avatar: Driver<UIImage?>
}
