//
//  UserTests.swift
//  subito-stargazersTests
//
//  Created by Enrico Querci on 12/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest

class UserTests: XCTestCase {
    
    var user: User!
    
    override func setUp() {
        super.setUp()
        self.user = User(username: "test_username", avatar: "test_avatar.png")
    }
    
    func testUsername() {
        XCTAssertEqual(user.username, "test_username")
    }
    
    func testAvatar() {
        XCTAssertEqual(user.avatar, "test_avatar.png")
    }
}
