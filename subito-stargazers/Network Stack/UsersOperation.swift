//
//  UsersOperation.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 05/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

class UsersOperation: Operation {
    typealias U = UserData

    private var owner: String
    private var repo: String
    private var nextPage: String?
    
    var request: URLRequest {
        if let nextPage = nextPage {
            return URLRequest(url: URL(string: nextPage)!)
        } else {
            return URLRequest(url: URL(string: "https://api.github.com/repos/\(owner)/\(repo)/stargazers")!)
        }
    }
    
    func execute(in dispatcher: Dispatcher) -> Observable<U> {
        return dispatcher.execute(request: request).map { response -> U in
            switch response {
                case .users(let users):
                    return users
                case .error(_, _):
                    return UserData(users: [], next: nil)
            default:
                fatalError("No other cases should be present here")
            }
        }
    }

    init(owner: String, repo: String) {
        self.owner = owner
        self.repo = repo
    }
    
    convenience init(nextPage: String) {
        self.init(owner: "", repo: "")
        self.nextPage = nextPage
    }
}
