//
//  UserDataTests.swift
//  subito-stargazersTests
//
//  Created by Enrico Querci on 12/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest

class UserDataTests: XCTestCase {
    
    var userData: UserData!
    
    override func setUp() {
        super.setUp()

        let userOne = User(username: "Test User One", avatar: "avatar.png")
        let userTwo = User(username: "Test User Two", avatar: "avatar.png")
        let userThree = User(username: "Test User Three", avatar: "avatar.png")
        
        userData = UserData(users: [userOne, userTwo, userThree], next: "http://next.link")
    }
    
    func testUserDataIsPopulated() {
        XCTAssertEqual(userData.users.count, 3)
        XCTAssertEqual(userData.users.first?.avatar, "avatar.png")
        XCTAssertEqual(userData.users.first?.username, "Test User One")
        XCTAssertEqual(userData.users[1].avatar, "avatar.png")
        XCTAssertEqual(userData.users[1].username, "Test User Two")
        XCTAssertEqual(userData.users.last?.avatar, "avatar.png")
        XCTAssertEqual(userData.users.last?.username, "Test User Three")
    }
    
    func testNextLink() {
        XCTAssertEqual(userData.next, "http://next.link")
    }
}
