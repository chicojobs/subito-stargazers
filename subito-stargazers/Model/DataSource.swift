//
//  DataSource.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 04/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

protocol DataSourceType {
    func getUsersFrom(_ repo: String, withOwner owner: String) -> Observable<UserData>
}

class DataSource: DataSourceType {
    let dispatcher: Dispatcher
    
    init(dispatcher: Dispatcher) {
        self.dispatcher = dispatcher
    }

    func getUsersFrom(_ repo: String, withOwner owner: String) -> Observable<UserData> {
        return UsersOperation(owner: owner, repo: repo).execute(in: dispatcher)
    }
    
    func getImageFor(_ urlString: String) -> Observable<Data> {
        return DataOperation(urlString: urlString).execute(in: dispatcher)
    }
    
    func getUsersFrom(nextPage: String)  -> Observable<UserData> {
        return UsersOperation(nextPage: nextPage).execute(in: dispatcher)
    }
}
