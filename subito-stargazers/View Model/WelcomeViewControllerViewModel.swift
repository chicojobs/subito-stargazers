//
//  WelcomeViewControllerViewModel.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 11/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

class WelcomeViewControllerViewModel {
    let searchButtonEnabled: Driver<Bool>
    
    init(ownerTap: Driver<String?>, repoTap: Driver<String?>) {
        self.searchButtonEnabled = Driver.combineLatest(ownerTap, repoTap).map { owner, repo in
            guard let owner = owner, owner != "", let repo = repo, repo != "" else {
                return false
            }
            
            let one = owner.components(separatedBy: " ").count
            let two = repo.components(separatedBy: " ").count
            
            return one < 2 && two < 2
        }
    }
}
