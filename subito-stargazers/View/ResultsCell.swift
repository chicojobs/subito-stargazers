//
//  ResultsCell.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 10/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

class ResultsCell: UITableViewCell {

    private let disposeBag = DisposeBag()

    var viewModel: ResultsCellViewModel? {
        didSet {
            if let vm = self.viewModel {
                setup(with: vm)
            }
        }
    }
    
    @IBOutlet weak private var avatarImageView: UIImageView!
    @IBOutlet weak private var userLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    func setup(with viewModel: ResultsCellViewModel) {
        viewModel.username.drive(userLabel.rx.text).addDisposableTo(disposeBag)
        viewModel.avatar.drive(onNext: { [weak self] image in
            self?.activityIndicator.stopAnimating()
            self?.avatarImageView.image = image
        }).addDisposableTo(disposeBag)
        
    }
    
    override func prepareForReuse() {
        avatarImageView.image = nil
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
}
