//
//  Response.swift
//  subito-stargazers
//
//  Created by Enrico Querci on 05/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

enum Response {
    case users(_: UserData)
    case data(_: Data)
    case error(_: Int?, _: Error?)
    
    init(_ response: (res: HTTPURLResponse?, data: Data?, error: Error?), for request: URLRequest) {
        guard let statusCode = response.res?.statusCode, case 200..<299 = statusCode else {
            self = .error(response.res?.statusCode, response.error)
            return
        }
        
        guard let data = response.data else {
            self = .error(response.res?.statusCode, NetworkErrors.noData)
            return
        }

        do {
            let decoder = JSONDecoder()
            let usersArray = try decoder.decode([User].self, from: data)
            var next: String?
            if let linkHeader = response.res?.allHeaderFields["Link"] as? String {
                next = linkHeader.components(separatedBy: "; rel=\"next\"").first
                next?.characters.removeFirst()
                next?.characters.removeLast()
            }
            self = .users(UserData(users: usersArray, next: next))
        } catch  {
            self = .data(data)
        }
    }
}

enum NetworkErrors: Error {
    case badInput
    case noData
}
