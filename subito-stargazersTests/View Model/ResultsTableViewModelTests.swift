//
//  ResultsTableViewModelTests.swift
//  subito-stargazersTests
//
//  Created by Enrico Querci on 12/09/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
import RxSwift
import RxTest

class ResultsTableViewModelTests: XCTestCase {
    
    var viewModel: ResultsTableViewModel!
    var disposeBag = DisposeBag()
    var scheduler: TestScheduler!

    override func setUp() {
        super.setUp()
        let userOne = User(username: "Test User One", avatar: "avatar.png")
        let userTwo = User(username: "Test User Two", avatar: "avatar.png")
        let userThree = User(username: "Test User Three", avatar: "avatar.png")

        let userData = UserData(users: [userOne, userTwo, userThree], next: "http://next.link")
        self.viewModel = ResultsTableViewModel(data: userData)
    }
    
    func testNumberOfUsers() {
        XCTAssertEqual(viewModel.numberOfUsers, 3)
    }

    func testNextLink() {
        XCTAssertEqual(viewModel.nextLink, "http://next.link")
    }
    
    func testResultCellViewModelForUserOne() {
        let userOne = User(username: "Test User One", avatar: "avatar.png")
        let mockResultsCellViewModel = ResultsCellViewModel(user: userOne)
        
        let resultsCellViewModel = viewModel.cellViewModel(for: IndexPath(row: 0, section: 0))
        XCTAssertNotNil(resultsCellViewModel)
        
        self.scheduler = TestScheduler(initialClock: 0)
        let stringObserver = scheduler.createObserver(String.self)

        scheduler.scheduleAt(0) {
            mockResultsCellViewModel.username.drive(stringObserver).addDisposableTo(self.disposeBag)
            resultsCellViewModel!.username.drive(stringObserver).addDisposableTo(self.disposeBag)
        }
        
        scheduler.start()
        
        // The two view models should emit the same seqence of events
        let expected = [
            next(0, "Test User One"),
            completed(0),
            next(0, "Test User One"),
            completed(0)
        ]
        
        XCTAssertEqual(stringObserver.events, expected)
    }
    
    func testAppendUserData() {
        let userFour = User(username: "Test User Four", avatar: "avatar.png")
        let newUserData = UserData(users: [userFour], next: "http://additional.next.link")
        
        XCTAssertEqual(viewModel.numberOfUsers, 3)
        viewModel.append(data: newUserData)
        XCTAssertEqual(viewModel.numberOfUsers, 4)
    }
}
